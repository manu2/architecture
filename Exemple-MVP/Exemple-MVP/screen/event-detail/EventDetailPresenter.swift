//
//  EventDetailPresenter.swift
//  Exemple-MVVM
//
//  Created by Emmanuel Orvain on 20/08/2021.
//

// View Model must not import UIKit
import Foundation

final class EventDetailPresenter{
    
    private var event: EventModel
    
    private(set) var eventTitle: String
    private(set) var eventSubtitle: String
    private(set) var eventPhotoData = Data()
    private(set) var eventPhoto: String
    private(set) var eventText: String
    private(set) var eventPosition: String = ""
    private(set) var eventDate: String = ""
    
    private lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        formatter.timeStyle = .none
        formatter.locale = Locale.current
        return formatter
    }()
    
    deinit {
        print("dealloc \(self)")
    }
    
    init(event: EventModel) {
        self.event = event
        self.eventTitle = event.title
        self.eventSubtitle = event.subtitle
        self.eventPhoto = event.photoPath
        self.eventText = event.textContent
        configurePosition()
        configureDate()
    }
    
    private func configurePosition(){
        eventPosition = String(format: "Lat : %.02f Long : %.02f", event.lat, event.long)
    }
    
    private func configureDate(){
        self.eventDate = dateFormatter.string(from: event.date)
    }
}
