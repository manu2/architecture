//
//  EventListViewController.swift
//  Exemple-MVVM
//
//  Created by Emmanuel Orvain on 23/08/2021.
//

import UIKit

final class EventListViewController: UIViewController{
    
    var presenter: EventListPresenter? {
        didSet{
            presenter?.delegate = self
            presenter?.fetchData()
        }
    }
    
    var spinner = UIActivityIndicatorView(style: .large)

    private lazy var collectionView: UICollectionView = {
        let view = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        view.dataSource = self
        view.delegate = self
        view.register(EventCollectionViewCell.self, forCellWithReuseIdentifier: "cellid")
        print("collection view built")
        return view
    }()
    
    private lazy var layout: UICollectionViewFlowLayout = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        //Will be overriden by UICollectionViewDelegateFlowLayout
        layout.sectionInset = UIEdgeInsets(top: 10, left: 10, bottom:10, right: 10)
        layout.itemSize = CGSize(width: 200, height: 150)
        layout.minimumLineSpacing = 10.0
        layout.minimumInteritemSpacing = 10.0
        print("layout built")
        return layout
    }()
    
    //MARK: - Lifecycle
    deinit{
        print("dealloc \(self)")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(collectionView)
        view.addSubview(spinner)
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(callAction))
        
        buildStyle()
        buildConstraints()
    }
        
    private func buildStyle(){
        title = "Event Collection"
        spinner.backgroundColor = UIColor(white: 0.5, alpha: 0.7)
        spinner.color = Theme.primaryColor
        collectionView.backgroundColor = Theme.backgroundColor
    }
    
    private func buildConstraints(){
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        spinner.translatesAutoresizingMaskIntoConstraints = false
        //let guide = view.safeAreaLayoutGuide
        let guide = view!
        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: guide.topAnchor),
            collectionView.bottomAnchor.constraint(equalTo: guide.bottomAnchor),
            collectionView.leftAnchor.constraint(equalTo: guide.leftAnchor),
            collectionView.rightAnchor.constraint(equalTo: guide.rightAnchor),
        ])
        
        NSLayoutConstraint.activate([
            spinner.centerXAnchor.constraint(equalTo: guide.centerXAnchor),
            spinner.centerYAnchor.constraint(equalTo: guide.centerYAnchor),
        ])
    }
    
    @objc
    func callAction(){
        presenter?.callAction()
    }
    
    private func showAlertView(){
        let controller = UIAlertController(title: Tr.t("Action panel"), message: Tr.t("Select action"), preferredStyle: .actionSheet)
        let newAction = UIAlertAction(title: Tr.t("New"),
                                      style: .default,
                                      handler: {[weak self] _ in
                                        //Implement creation of an event
                                        print("The creation of an event is not yet implemented")
                                      })
        let refreshAction = UIAlertAction(title: Tr.t("Refresh"),
                                         style: .default,
                                         handler: {[weak self] _ in
                                            self?.presenter?.fetchData()})
       
        controller.addAction(newAction)
        controller.addAction(refreshAction)

        if let popoverPresentationController = controller.popoverPresentationController {
            popoverPresentationController.sourceView = view
            popoverPresentationController.sourceRect = view.bounds
        }

        self.present(controller, animated: true) {
            //done
        }
    }
}

extension EventListViewController: UICollectionViewDelegate{
    
}

extension EventListViewController: UICollectionViewDataSource{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return presenter?.numberOfSection() ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter?.numberOfRowsInSection(section: section) ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellid", for: indexPath)
        
        guard let eventCell = cell as? EventCollectionViewCell,
              let model = presenter?.modelAt(index: indexPath) else{
            return cell
        }

        let adapter = EventCellAdapter(cell: eventCell, model: model)
        adapter.adapt()

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let model = presenter?.modelAt(index: indexPath){
            let presenter = EventDetailPresenter(event: model)
            let vc = EventDetailViewController()
            vc.presenter = presenter
            navigationController?.pushViewController(vc, animated: true)
        }
    }
}

extension EventListViewController : EventListView{
    func onStartFetch() {
        self.spinner.startAnimating()
    }
    
    func onDataFetched(data: [EventModel]) {
        DispatchQueue.main.async {
                self.spinner.stopAnimating()
                self.collectionView.reloadData()
        }
    }
    
    func onPresentActionList() {
        showAlertView()
    }
}

#if DEBUG && canImport(SwiftUI) && canImport(Combine)
import SwiftUI

@available(iOS 13.0, *)
struct EventListViewControllerRepresentable: UIViewRepresentable {
  func makeUIView(context: Context) -> UIView {
    let presenter = EventListPresenter()
    let vc = EventListViewController()
    vc.presenter = presenter
    vc.view.backgroundColor = .brown
    return vc.view
  }
  
  func updateUIView(_ view: UIView, context: Context) {
  }
}

@available(iOS 13.0, *)
struct EventListViewControllerRepresentablePreview: PreviewProvider {
  static var previews: some View {
    EventListViewControllerRepresentable().previewDevice(PreviewDevice(rawValue: "iPhone SE (2nd generation)"))
  }
}
#endif
