//
//  EventListPresenter.swift
//  Exemple-MVVM
//
//  Created by Emmanuel Orvain on 23/08/2021.
//

//ViewModel must not import UIKit
import Foundation

protocol EventListView: class {
    func onStartFetch()
    func onDataFetched(data :[EventModel])
    func onPresentActionList()
}

final class  EventListPresenter{

    weak var delegate: EventListView?
    
    private var data = [EventModel]()
    
    private let webservice = Webservice()
    
    init(){
        fetchData()
    }
    
    func fetchData(){
        delegate?.onStartFetch()
        webservice.fetchData {[weak self] (eventList) in
            self?.data = eventList
            self?.delegate?.onDataFetched(data: eventList)
        }
    }
    
    func numberOfSection()->Int{
        return 1
    }
    
    func numberOfRowsInSection(section: Int) -> Int {
        return data.count
    }
    
    func modelAt(index: IndexPath) -> EventModel{
        return data[index.row]
    }
    
    @objc
    func callAction(){
        delegate?.onPresentActionList()
    }
    
}
