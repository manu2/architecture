//
//  Webservice.swift
//  Exemple-MVVM
//
//  Created by EmEmmanuel Orvainel Orvain on 24/08/2021.
//

import Foundation

final class Webservice{
    
    func fetchData(completionHandler: @escaping (([EventModel]) -> ())){
        //Mock a webservice call, fake ping
        DispatchQueue.global(qos: .userInteractive).asyncAfter(deadline: .now() + 1){
            completionHandler(EventMock.readEventListFile())
        }
    }
}
