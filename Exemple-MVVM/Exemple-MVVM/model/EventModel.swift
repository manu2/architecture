//
//  EventModel.swift
//  Exemple-MVVM
//
//  Created by EmEmmanuel Orvainel Orvain on 20/08/2021.
//

import Foundation

struct EventModel: Decodable{
    //var id: String = ""
    var photoPath: String = ""
    var title: String = ""
    var subtitle: String = ""
    var textContent: String = ""
    var long: Double = 0.0
    var lat: Double = 0.0
    var date: Date = Date()
}
