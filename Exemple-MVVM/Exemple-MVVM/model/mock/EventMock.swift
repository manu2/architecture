//
//  EventMock.swift
//  Exemple-MVVM
//
//  Created by EmEmmanuel Orvainel Orvain on 20/08/2021.
//

import Foundation

struct EventMock{
    static func readEventListFile() -> [EventModel]{
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .secondsSince1970
        if let path = Bundle.main.path(forResource: "event_list", ofType: "json"),
           let data = try? Data(contentsOf: URL(fileURLWithPath: path)),
           let eventList = try? decoder.decode([EventModel].self, from: data){
            print("Load mocked data")
            return eventList
        }
        else{
            return [EventModel]()
        }
    }
}
