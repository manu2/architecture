//
//  Translation.swift
//  Exemple-MVVM
//
//  Created by EmEmmanuel Orvainel Orvain on 24/08/2021.
//

import Foundation

struct Tr{
    static func t(_ key:String)->String{
        return NSLocalizedString(key, comment: "")
    }
}
