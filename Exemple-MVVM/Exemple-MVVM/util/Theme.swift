//
//  Theme.swift
//  Exemple-MVVM
//
//  Created by EmEmmanuel Orvainel Orvain on 20/08/2021.
//

import Foundation
import UIKit

struct Theme{
    static let defaultFont = UIFont.systemFont(ofSize: UIFont.systemFontSize)
    static let font = UIFont(name: "Baskerville", size: 20) ?? defaultFont
    static let bookFont = UIFont(name: "Baskerville-SemiBold", size: 20)
    static let h1Font: UIFont = font.withSize(40.0)
    static let h2Font: UIFont = font.withSize(20.0)
    static let footerFont: UIFont = font.withSize(12)
    static let buttonFont: UIFont = font.withSize(25.0)
    
    static let primaryColor = UIColor.fern
    //Color of uitableview grouped background color
    static let headerColor: UIColor = UIColor.RGB(242, 242, 247)
    static let backgroundColor: UIColor = UIColor(patternImage: UIImage(named: "background-standard.png")!)
    static let backgroundPaymentColor: UIColor = backgroundColor
    // Cell colors
    static let cellColor1 =  UIColor.RGB(236, 236, 236)
    static let cellColor2 =  UIColor.white
    // IAP
    static let iapGoldColor = UIColor.RGB(255, 175, 40)
    static let iapTitleColor = UIColor.RGB(147,0,14)
    
    // MARK: - Dimensions
    static let padding: CGFloat = 5.0
    static let paddingInverted: CGFloat = -5.0
    
    // MARK: - Shadows
    static func addShadow(view: UIView, radius: CGFloat = 2.0, offset: CGSize = CGSize(width: 2, height: 2)){
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowOpacity = 0.8
        view.layer.shadowRadius = radius
        view.layer.shadowOffset = offset
    }
    
    // MARK: - Navigation bar
    static func applyStyleNavigation(_ vc: UIViewController) {
        if let subvc = vc as? UINavigationController{
            let bar = subvc.navigationBar
            bar.barTintColor = .white
            bar.tintColor = primaryColor
            let font = Theme.font.withSize(18.0)
            bar.titleTextAttributes = [NSAttributedString.Key.font : font,
                                       NSAttributedString.Key.foregroundColor : UIColor.black]
        }
    }
}

extension UIColor{
    
    static func RGB(_ r: Int, _ g: Int, _ b: Int, _ alpha: Int = 255)->UIColor{
        return UIColor(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: CGFloat(alpha) / 255.0)
    }
}
