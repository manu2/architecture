//
//  UIColor+util.swift
//  Exemple-MVVM
//
//  Created by EmEmmanuel Orvainel Orvain on 20/08/2021.
//

import UIKit

extension UIColor{
    static var cantaloupe:UIColor{
        get{
            return UIColor.colorRgb255(255, 204, 102, 255)
        }
    }
    static var tungstene:UIColor{
        get{
            return UIColor.colorRgb255(50, 50, 50, 255)
        }
    }
    
    static var iron:UIColor{
        get{
            return UIColor.colorRgb255(76, 76, 76, 255)
        }
    }
    
    static var mercury:UIColor{
        get{
            return UIColor.colorRgb255(230, 230, 230, 255)
        }
    }
    
    static var honeydew:UIColor{
        get{
            return UIColor.colorRgb255(204, 255, 102, 255)
        }
    }
    
    static var fern:UIColor{
        get{
            return UIColor.colorRgb255(64, 128, 0, 255)
        }
    }
    
    public class func colorRgb255(_ red:Float, _ green:Float, _ blue:Float, _ alpha:Float)->UIColor{
        return UIColor(red: CGFloat(red/255.0),
                       green:CGFloat(green/255.0),
                       blue:CGFloat(blue/255.0),
                       alpha:CGFloat(alpha/255.0))
    }
    

}
