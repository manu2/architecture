//
//  EventDetailViewController.swift
//  Exemple-MVVM
//
//  Created by Emmanuel Orvain on 20/08/2021.
//

import UIKit

final class EventDetailViewController: UIViewController{
    
    /// - UI
    let scrollView = UIScrollView()
    let contentView = UIView()
    
    let titleLabel = UILabel()
    let subtitleLabel = UILabel()
    let photoView = UIImageView()
    let latLongLabel = UILabel()
    let dateLabel = UILabel()
    let textLabel = UILabel()
    
    var viewModel: EventDetailViewModel?
    
    deinit {
        print("dealloc \(self)")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(scrollView)
        scrollView.addSubview(contentView)
        
        contentView.addSubview(photoView)
        contentView.addSubview(titleLabel)
        contentView.addSubview(subtitleLabel)
        contentView.addSubview(textLabel)
        contentView.addSubview(latLongLabel)
        contentView.addSubview(dateLabel)
        
        bindViewModel()
        buildStyle()
        buildConstraints()
        
        view.setNeedsUpdateConstraints()
    }
    
    private func buildStyle(){
        title = "Event"
        view.backgroundColor = Theme.backgroundColor
        photoView.contentMode = .scaleAspectFill
        photoView.layer.masksToBounds = true
        photoView.layer.cornerRadius = 8.0
        
        titleLabel.font = Theme.h1Font
        titleLabel.numberOfLines = 0
        
        subtitleLabel.font = Theme.h2Font
        subtitleLabel.numberOfLines = 0
        
        dateLabel.font = Theme.footerFont
        dateLabel.textAlignment = .right
        latLongLabel.font = Theme.footerFont
        latLongLabel.textAlignment = .right
        
        textLabel.font = Theme.defaultFont
        textLabel.numberOfLines = 0
        textLabel.backgroundColor = .white
    }
    
    private func buildConstraints(){
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        contentView.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        subtitleLabel.translatesAutoresizingMaskIntoConstraints = false
        dateLabel.translatesAutoresizingMaskIntoConstraints = false
        latLongLabel.translatesAutoresizingMaskIntoConstraints = false
        photoView.translatesAutoresizingMaskIntoConstraints = false
        textLabel.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: view.topAnchor),
            scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            scrollView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            scrollView.widthAnchor.constraint(equalTo: view.widthAnchor),
        ])
        
        NSLayoutConstraint.activate([
            contentView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            contentView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
            contentView.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor),
            contentView.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: Theme.paddingInverted * 2),
        ])
        
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: Theme.padding),
            titleLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor),
            titleLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor)
        ])
        
        NSLayoutConstraint.activate([
            subtitleLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: Theme.padding),
            subtitleLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor),
            subtitleLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor),
        ])
        
        NSLayoutConstraint.activate([
            dateLabel.topAnchor.constraint(equalTo: subtitleLabel.bottomAnchor, constant: Theme.padding),
            dateLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor),
        ])
        
        NSLayoutConstraint.activate([
            latLongLabel.topAnchor.constraint(equalTo: dateLabel.bottomAnchor, constant: Theme.padding),
            latLongLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor),
        ])
        
        NSLayoutConstraint.activate([
            photoView.topAnchor.constraint(equalTo: dateLabel.bottomAnchor, constant: 30),
            photoView.heightAnchor.constraint(equalToConstant: 150),
            photoView.leftAnchor.constraint(equalTo: contentView.leftAnchor),
            photoView.rightAnchor.constraint(equalTo: contentView.rightAnchor)
        ])
        
        NSLayoutConstraint.activate([
            textLabel.topAnchor.constraint(equalTo: photoView.bottomAnchor, constant: 30),
            textLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor),
            textLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor),
            textLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: Theme.paddingInverted)
        ])
    }
    
    private func bindViewModel(){
        viewModel?.eventTitle.bindAndFire({ [weak self] (text) in
            self?.titleLabel.text = text
        })
        viewModel?.eventSubtitle.bindAndFire({[weak self] (text) in
            self?.subtitleLabel.text = text
        })
        viewModel?.eventDate.bindAndFire({ [weak self] (text) in
            self?.dateLabel.text = text
        })
        viewModel?.eventPosition.bindAndFire({ [weak self] (text) in
            self?.latLongLabel.text = text
        })
        viewModel?.eventPhoto.bindAndFire({ [weak self] (imageFilename) in
            self?.photoView.image = UIImage(named: imageFilename)
        })
        viewModel?.eventText.bindAndFire({ [weak self] text in
            self?.textLabel.text = text
        })
    }
}
#if DEBUG && canImport(SwiftUI) && canImport(Combine)
import SwiftUI

@available(iOS 13.0, *)
struct EventDetailViewControllerRepresentable: UIViewRepresentable {
    func makeUIView(context: Context) -> UIView {
        let model = EventMock.readEventListFile()[1]
        let viewModel = EventDetailViewModel(event: model)
        let vc = EventDetailViewController()
        vc.viewModel = viewModel
        return vc.view
    }
    
    func updateUIView(_ view: UIView, context: Context) {
    }
}

@available(iOS 13.0, *)
struct EventDetailViewControllerPreview: PreviewProvider {
    static var previews: some View {
        //BookViewControllerRepresentable()
        EventDetailViewControllerRepresentable().previewDevice(PreviewDevice(rawValue: "iPhone SE (1st generation)"))
    }
}
#endif
