//
//  EventDetailViewModel.swift
//  Exemple-MVVM
//
//  Created by Emmanuel Orvain on 20/08/2021.
//

// View Model must not import UIKit
import Foundation

final class EventDetailViewModel{
    
    private var event: EventModel
    
    private(set) var eventTitle = Dynamic<String>("")
    private(set) var eventSubtitle = Dynamic<String>("")
    private(set) var eventPhotoData = Dynamic<Data>(Data())
    private(set) var eventPhoto = Dynamic<String>("")
    private(set) var eventText = Dynamic<String>("")
    private(set) var eventPosition = Dynamic<String>("")
    private(set) var eventDate = Dynamic<String>("")
    
    private lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        formatter.timeStyle = .none
        formatter.locale = Locale.current
        return formatter
    }()
    
    deinit {
        print("dealloc \(self)")
    }
    
    init(event: EventModel) {
        self.event = event
        self.eventTitle.value = event.title
        self.eventSubtitle.value = event.subtitle
        self.eventPhoto.value = event.photoPath
        self.eventText.value = event.textContent
        configurePosition()
        configureDate()
    }
    
    private func configurePosition(){
        eventPosition.value = String(format: "Lat : %.02f Long : %.02f", event.lat, event.long)
    }
    
    private func configureDate(){
        self.eventDate.value = dateFormatter.string(from: event.date)
    }
}
