//
//  EventListViewModel.swift
//  Exemple-MVVM
//
//  Created by Emmanuel Orvain on 23/08/2021.
//

//ViewModel must not import UIKit
import Foundation

final class  EventListViewModel{
    //dynamic
    private(set) var isFetching = Dynamic<Bool>(true)
    private(set) var dataFetched = Dynamic<Bool>(true)
    private(set) var presentActionList = Dynamic<Bool>(true)
    
    private var data = [EventModel]()
    
    private let webservice = Webservice()
    
    init(){
        fetchData()
    }
    
    func fetchData(){
        isFetching.value = true
        webservice.fetchData {[weak self] (eventList) in
            self?.data = eventList
            self?.dataFetched.value = true
            self?.isFetching.value = false
        }
    }
    
    func numberOfSection()->Int{
        return 1
    }
    
    func numberOfRowsInSection(section: Int) -> Int {
        return data.count
    }
    
    func modelAt(index: IndexPath) -> EventModel{
        return data[index.row]
    }
    
    @objc
    func callAction(){
        presentActionList.value = true
    }
    
}
