//
//  EventCollectionViewCell.swift
//  Exemple-MVVM
//
//  Created by Emmanuel Orvain on 23/08/2021.
//

import UIKit

final class EventCollectionViewCell : UICollectionViewCell{
    
    private(set) var titleLabel = UILabel()
    private(set) var imageView = UIImageView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildSubviews()
        buildConstraints()
        buildStyle()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        print("dealloc \(self)")
    }
    
    private func buildSubviews(){
        contentView.addSubview(imageView)
        contentView.addSubview(titleLabel)
    }
    
    private func buildConstraints(){
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        imageView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            titleLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            titleLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor),
            titleLabel.widthAnchor.constraint(equalTo: contentView.widthAnchor),
        ])
        NSLayoutConstraint.activate([
            imageView.topAnchor.constraint(equalTo: contentView.topAnchor),
            imageView.heightAnchor.constraint(equalTo: contentView.heightAnchor),
            imageView.leftAnchor.constraint(equalTo: contentView.leftAnchor),
            imageView.widthAnchor.constraint(equalTo: contentView.widthAnchor),
        ])

    }
    
    private func buildStyle(){
        titleLabel.font = Theme.h2Font
        titleLabel.textAlignment = .center
        titleLabel.backgroundColor = UIColor.white.withAlphaComponent(0.75)
        contentView.layer.cornerRadius = 8.0
        contentView.layer.masksToBounds = true
        imageView.contentMode = .scaleAspectFill
        Theme.addShadow(view: self)
    }
}

#if DEBUG && canImport(SwiftUI) && canImport(Combine)
import SwiftUI

@available(iOS 13.0, *)
struct EventCollectionViewCellRepresentable: UIViewRepresentable {
  func makeUIView(context: Context) -> UIView {
    let cell = EventCollectionViewCell()
    cell.titleLabel.text = "Hello World!"
    cell.imageView.image = UIImage(named: "pier-569314__480.jpg")
    return cell
  }
  
  func updateUIView(_ view: UIView, context: Context) {
  }
}

@available(iOS 13.0, *)
struct EventCollectionViewCellRepresentablePreview: PreviewProvider {
  static var previews: some View {
    EventCollectionViewCellRepresentable().frame(width: 150, height: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
  }
}
#endif
