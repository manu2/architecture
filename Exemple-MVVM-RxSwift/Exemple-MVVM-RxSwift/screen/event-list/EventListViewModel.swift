//
//  EventListViewModel.swift
//  Exemple-MVVM
//
//  Created by Emmanuel Orvain on 23/08/2021.
//

//ViewModel must not import UIKit
import Foundation
import RxSwift

final class  EventListViewModel{
    //dynamic
    private(set) var isFetching = BehaviorSubject<Bool>(value: false)
    private(set) var dataFetched = PublishSubject<Bool>()
    private(set) var presentActionList = PublishSubject<Bool>()
    
    private var data = [EventModel]()
    private var filteredData = [EventModel]()
    
    private let webservice = Webservice()
    
    init(){
        fetchData()
    }
    
    func fetchData(){
        isFetching.onNext(true)
        webservice.fetchData {[weak self] (eventList) in
            self?.data = eventList
            self?.filteredData = eventList
            self?.dataFetched.onNext(true)
            self?.isFetching.onNext(false)
        }
    }
    
    func filterData(searchText: String?) {
        defer {
            dataFetched.onNext(true)
        }
        guard let searchText = searchText,
              searchText.count > 0 else{
            filteredData = data
            return
        }
        filteredData = data.filter { (model) -> Bool in
            return model.title.range(of: searchText, options: .caseInsensitive) != nil
        }
    }
    
    func numberOfSection()->Int{
        return 1
    }
    
    func numberOfRowsInSection(section: Int) -> Int {
        return filteredData.count
    }
    
    func modelAt(index: IndexPath) -> EventModel{
        return filteredData[index.row]
    }
    
    @objc
    func callAction(){
        presentActionList.onNext(true)
    }
}
