//
//  EventCellAdapter.swift
//  Exemple-MVVM
//
//  Created by Emmanuel Orvain on 23/08/2021.
//

import UIKit

final class EventCellAdapter{
    let cell: EventCollectionViewCell
    let model: EventModel
    
    init(cell: EventCollectionViewCell, model: EventModel){
        self.cell = cell
        self.model = model
    }
    
    func adapt(){
        cell.titleLabel.text = model.title
        cell.imageView.image = UIImage(named:model.photoPath)
    }
}
