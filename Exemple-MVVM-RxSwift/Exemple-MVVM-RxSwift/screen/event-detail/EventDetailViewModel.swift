//
//  EventDetailViewModel.swift
//  Exemple-MVVM
//
//  Created by Emmanuel Orvain on 20/08/2021.
//

// View Model must not import UIKit
import Foundation
import RxSwift

final class EventDetailViewModel{
    
    private var event: EventModel
    
    private(set) var eventTitle = BehaviorSubject<String>(value: "")
    private(set) var eventSubtitle = BehaviorSubject<String>(value: "")
    private(set) var eventPhotoData = BehaviorSubject<Data>(value: Data())
    private(set) var eventPhoto = BehaviorSubject<String>(value: "")
    private(set) var eventText = BehaviorSubject<String>(value: "")
    private(set) var eventPosition = BehaviorSubject<String>(value: "")
    private(set) var eventDate = BehaviorSubject<String>(value: "")
    
    private lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        formatter.timeStyle = .none
        formatter.locale = Locale.current
        return formatter
    }()
    
    deinit {
        print("dealloc \(self)")
    }
    
    init(event: EventModel) {
        self.event = event
        self.eventTitle.onNext(event.title)
        self.eventSubtitle.onNext(event.subtitle)
        self.eventPhoto.onNext(event.photoPath)
        self.eventText.onNext(event.textContent)
        configurePosition()
        configureDate()
    }
    
    private func configurePosition(){
        let formattedPosition = String(format: "Lat : %.02f Long : %.02f", event.lat, event.long)
        eventPosition.onNext(formattedPosition)
    }
    
    private func configureDate(){
        eventDate.onNext(dateFormatter.string(from: event.date))
    }
}
