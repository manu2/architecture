//
//  rxplayground.swift
//  Exemple-MVVM-RxSwift
//
//  Created by Manu on 26/08/2021.
//

import Foundation
import RxSwift

class RxPlayground{
    
    static func test(){
        let observable = Observable.of(1, 2, 3, 4)
        observable.subscribe{ event in
            print(event)
            if let element = event.element{
                print(event.element)
            }
        }
    }
}
