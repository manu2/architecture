# Architecture

The intent of this project is to show how to handle application development for the same application but with different swoftware architecture.

All this archtecture are on top of Swift language.



## MVVM 
*Model-View-ViewModel*

Model–view–viewmodel (MVVM) is a software architectural pattern that facilitates the separation of the development of the graphical user interface (the view) – be it via a markup language or GUI code – from the development of the business logic or back-end logic (the model) so that the view is not dependent on any specific model platform. The viewmodel of MVVM is a value converter, meaning the viewmodel is responsible for exposing (converting) the data objects from the model in such a way that objects are easily managed and presented. In this respect, the viewmodel is more model than view, and handles most if not all of the view's display logic. The viewmodel may implement a mediator pattern, organizing access to the back-end logic around the set of use cases supported by the view.

## MVVM-RxSwift
*MVVM with RxSwift Reactive framework*

Rx is a generic abstraction of computation expressed through Observable<Element> interface, which lets you broadcast and subscribe to values and other events from an Observable stream.

RxSwift is the Swift-specific implementation of the Reactive Extensions standard.

## MVP
*Model View Presenter*

MVP is a user interface architectural pattern engineered to facilitate automated unit testing and improve the separation of concerns in presentation logic:

The model is an interface defining the data to be displayed or otherwise acted upon in the user interface.
The view is a passive interface that displays data (the model) and routes user commands (events) to the presenter to act upon that data.
The presenter acts upon the model and the view. It retrieves data from repositories (the model), and formats it for display in the view.


## Swift UI

SwiftUI is a modern way to declare user interfaces for any Apple platform. Create beautiful, dynamic apps faster than ever before.

*TODO*

## References

[Swift language refrence](https://swift.org/)

[MVVM architecture reference](https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93viewmodel)

[RxSwift reference](https://github.com/ReactiveX/RxSwift)

[MVP architecture reference](https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93presenter)

[SwiftUI](https://developer.apple.com/documentation/swiftui/)

# Application

This is a basic application, the goal is to demonstrates the separation of concern between each architecture element.

## Home screen

The home screen shows a collection view with data fetched from a fake Json werbservice. The model are a list of event (title, subtitle, content, content, geoposition, ...) 

* Scroll over the event list and select a cell
* Refresh the data from the webservice

**UI**

* Autolayout without storyboard
* Use Preview in Xcode to show the views without running it on a simulator

## Detail Event Screen

The detail event screen shows all the data related to the event in a scroll view.

**UI**

* Autolayout without storyboard
* Use Preview in Xcode to show the views without running it on a simulator

